const tar = 'Sala Arq. Jorge Tartarini'

const tooltipTexts = {
  intro: {
    title: 'Visitá el Museo del Agua',
    text:
      'Si por fuera el Palacio de las Aguas Corrientes es asombroso, te invitamos a hacer un recorrido virtual por nuestro Museo, para sorprenderte con lo que guarda en su interior este Gran Depósito Distribuidor, construido a fines del siglo XIX.' +
      '\n' +
      '\n' +
      'Conocé la historia de las obras de salubridad, su magnífico patrimonio y el proyecto y construcción de la singular sede de nuestro Museo, símbolo de la importancia otorgada por la Nación a la higiene pública y, en particular, al agua “pura”.',
  },
  maqueta: {
    title: 'Maqueta Ciudad de Buenos Aires',
    text:
      'Construída en el año 1933 por operarios de la empresa Obras Sanitarias de la Nación. Realizada en dos escalas: una horizontal y una vertical, muy exagerada para que se noten mejor las diferencias de alturas del terreno.',
  },
  fachada: {
    title: 'Piezas de la fachada',
    text:
      'Este edificio esta totalmente revestido por piezas de terracota inglesa, esmaltadas y vitrificadas. Cada una de ellas posee una letra y un número que corresponde a su ubicación en la fachada. Las piezas exhibidas son las que se guardaron para ser utilizadas como repuesto.',
  },
  materiales: {
    title: 'Materiales y Artefactos Sanitarios',
    text:
      'Materiales y Artefactos Sanitarios. Se exhiben artefactos importados y nacionales. Estos ejemplares eran sometidos a diversas pruebas para su futura aprobación y posterior comercialización. Como por ejemplo, griferías, piezas especiales, inodoros, mingitorios, vaciaderos, entre otros.',
  },
  oficina: {
    title: 'Recreación de “Oficina de Aprobación de Materiales”',
    text:
      'Se dedicaba a controlar la calidad de artefactos y materiales sanitarios que aspiraban a ser comercializados en el país. Aquí recreamos el ambiente de trabajo, con sus escritorios de madera, grandes ficheros, tablero de dibujo con taburete, planeras, etc.',
  },
  distribución: {
    title: 'Evolución del Sistema de distribución de Agua',
    text:
      'Con el correr de los años y el avance de la tecnología, el sistema de presión utilizado se reemplazó por el llamado “Sistema de Ríos Subterráneos”',
  },
  metal_a: {
    title: 'Estructura metálica - Parte 1',
    text:
      'Este es el verdadero corazón del Palacio, sostenido por una malla de 180 columnas metálicas que vinieron de Bélgica. Soportaba el peso de los 12 tanques de agua con una capacidad de más de 72 millones de litros.',
  },
  colonial: {
    title: 'Recreación Cuadra Colonial',
    text:
      'Sector ambientado para recordar la Buenos Aires de comienzos del siglo XIX y el precario sistema de distribución de agua de la época, como el aguatero y el aljibe.',
  },
  temporarias: {
    title: 'Exposiciones temporarias',
    text:
      'En este espacio se muestran diferentes expresiones culturales, cuyo hilo conductor es el patrimonio del agua y su relación con el cuidado del medio ambiente.',
  },
  metal_b: {
    title: 'Estructura metálica - Parte 2',
    text:
      'Por esta cañería maestra se distribuía el agua que venía impulsada por las bombas a vapor desde el Establecimiento Recoleta e ingresaba a los tanques para su posterior distribución en el Radio Antiguo de la Ciudad de Buenos Aires.',
  },
  balcon: {
    title: 'Balcón Primer Piso',
    text:
      'Aquí contemplamos un variado contraste de piezas decorativas, a las que se suman cariátides de hierro fundido ubicadas a los costados de las ventanas del cuerpo central sobre Ayacucho, provistas por la firma W. Macfarrlane & Co., de Glasgow, Escocia.',
  },
  archivo: {
    title: 'Archivo de planos históricos',
    text:
      'Archivo de planos desde 1867 a la actualidad de todas las obras de saneamiento del país. ',
  },
}

const buildTooltip = (text) => (clickHandlerArgs) => {
  const { target: root } = clickHandlerArgs
  if (
    root.childNodes.length > 1 ||
    root.tagName === 'P' ||
    root.tagName === 'SPAN'
  ) {
    let t = document.querySelector('.tooltip')
    t.classList.add('out')
    setTimeout((_) => {
      t.remove()
    }, 250)
    return
  }
  const tooltip = document.createElement('p')
  tooltip.classList.add('tooltip')
  tooltip.innerText = text
  root.appendChild(tooltip)
}

// ENTRADA
const entrada_a = {
  title: 'Entrada',
  pitch: -2,
  yaw: -23,
  centerPitch: 2,
  centerYaw: -28,
  type: 'equirectangular',
  panorama: 'data/img/tour/entrada_a.jpg',
  hotSpots: [
    {
      pitch: -39,
      yaw: -28,
      centerPitch: -2,
      centerYaw: -22,
      type: 'scene',
      text: 'Entrada',
      sceneId: 'entrada_b',
    },
  ],
}

const entrada_b = {
  title: 'Entrada',
  pitch: -7,
  yaw: -11,
  centerPitch: -3,
  centerYaw: -7,
  type: 'equirectangular',
  panorama: 'data/img/tour/entrada_b.jpg',
  hotSpots: [
    {
      type: 'scene',
      sceneId: 'entrada_a',
      text: 'Entrada',
      pitch: -33,
      yaw: 131,
      centerPitch: -12.0,
      centerYaw: 143,
    },
    {
      type: 'scene',
      sceneId: 'sala1_a',
      text: 'Sala 1',
      pitch: -28,
      yaw: -7,
      centerPitch: -7,
      centerYaw: -10,
    },
    {
      type: 'info',
      text: tooltipTexts['maqueta'].title,
      pitch: -20,
      yaw: -148,
      centerPitch: -20,
      centerYaw: -157,
      clickHandlerFunc: buildTooltip(tooltipTexts['maqueta'].text),
    },
  ],
}

//Sala 1

const sala1_a = {
  title: 'Sala 1',
  pitch: -1.31,
  yaw: 60,
  centerPitch: -1.7,
  centerYaw: 92,
  type: 'equirectangular',
  panorama: 'data/img/tour/sala1_a.jpg',
  hotSpots: [
    {
      type: 'scene',
      sceneId: 'entrada_b',
      text: 'Entrada',
      pitch: -20,
      yaw: -111,
      centerPitch: -15.8,
      centerYaw: 272.7,
    },
    {
      type: 'scene',
      sceneId: 'sala1_b',
      text: 'Sala 1',
      pitch: -11.4,
      yaw: 60.5,
      centerPitch: -1.31,
      centerYaw: 94,
    },
    {
      type: 'info',
      text: tooltipTexts['fachada'].title,
      pitch: -10,
      yaw: -33,
      centerPitch: -9,
      centerYaw: -26,
      clickHandlerFunc: buildTooltip(tooltipTexts['fachada'].text),
    },
  ],
}

const sala1_b = {
  title: 'Sala 1',
  pitch: -1.31,
  yaw: -114,
  centerPitch: -1.7,
  centerYaw: 92,
  type: 'equirectangular',
  panorama: 'data/img/tour/sala1_b.jpg',
  hotSpots: [
    {
      type: 'scene',
      sceneId: 'sala1_a',
      text: 'Sala 1',
      pitch: -13,
      yaw: 64,
      centerPitch: -10.4,
      centerPitch: -93.2,
    },
    {
      type: 'scene',
      sceneId: 'sala1_c',
      text: 'Sala 1',
      pitch: -20,
      yaw: -116.2,
      centerPitch: -3.4,
      centerYaw: 94.6,
    },
  ],
}

const sala1_c = {
  title: 'Sala 1',
  type: 'equirectangular',
  panorama: 'data/img/tour/sala1_c.jpg',
  pitch: 0,
  yaw: -126,
  centerPitch: 0,
  centerYaw: -126,
  hotSpots: [
    {
      type: 'scene',
      sceneId: 'sala1_b',
      text: 'Sala 1',
      pitch: -9,
      yaw: 146,
      centerPitch: -5,
      centerYaw: -216,
    },
    {
      type: 'scene',
      sceneId: 'sala2_a',
      text: 'Sala 2',
      pitch: -21.15,
      yaw: -138.4,
      centerPitch: -3.1,
      centerYaw: -114.3,
    },
    {
      type: 'info',
      text: tooltipTexts['materiales'].title,
      pitch: -3,
      yaw: -43,
      centerPitch: -7,
      centerYaw: -41,
      clickHandlerFunc: buildTooltip(tooltipTexts['materiales'].text),
    },
  ],
}

// SALA 2
const sala2_a = {
  title: 'Sala 2',
  type: 'equirectangular',
  panorama: 'data/img/tour/sala2_a.jpg',
  pitch: 1.6,
  yaw: -119.5,
  centerPitch: 0.4,
  centerYaw: 236.75,
  hotSpots: [
    {
      type: 'scene',
      sceneId: 'sala1_c',
      text: 'Sala 1',
      pitch: -21,
      yaw: 59.4,
      centerPitch: -9.6,
      centerYaw: 59.72,
    },
    {
      type: 'scene',
      sceneId: 'sala2_b',
      text: 'Sala 3',
      pitch: -20,
      yaw: -110.98,
      centerPitch: 0.6,
      centerYaw: 242.5,
    },
    {
      type: 'info',
      text: tooltipTexts['oficina'].title,
      pitch: -10,
      yaw: 134,
      centerPitch: -12,
      centerYaw: -220,
      clickHandlerFunc: buildTooltip(tooltipTexts['oficina'].text),
    },
  ],
}

const sala2_b = {
  title: 'Sala 3',
  type: 'equirectangular',
  panorama: 'data/img/tour/sala2_b.jpg',
  pitch: -2,
  yaw: 152,
  centerPitch: -1,
  centerYaw: -211,
  hotSpots: [
    {
      type: 'scene',
      sceneId: 'sala2_a',
      text: 'Sala 2',
      pitch: -10.4,
      yaw: -128,
      centerPitch: 2.6,
      centerYaw: -117.3,
    },
    {
      type: 'scene',
      sceneId: 'sala2_c',
      text: 'Sala 3',
      pitch: -17,
      yaw: 162,
      centerPitch: -2,
      centerYaw: 165,
    },
  ],
}

const sala2_c = {
  title: 'Sala 3',
  type: 'equirectangular',
  panorama: 'data/img/tour/sala2_c.jpg',
  pitch: 7,
  yaw: 94,
  centerPitch: 8,
  centerYaw: 89,
  hotSpots: [
    {
      type: 'scene',
      sceneId: 'sala2_b',
      text: 'Sala 3',
      pitch: -12,
      yaw: 156,
      centerPitch: 8,
      centerYaw: 180,
    },
    {
      type: 'scene',
      sceneId: 'sala3_a',
      text: tar,
      pitch: -13,
      yaw: 82,
      centerPitch: 0,
      centerYaw: 97,
    },
    {
      type: 'info',
      text: tooltipTexts['distribución'].title,
      pitch: 1,
      yaw: -90,
      centerPitch: 3,
      centerYaw: -86,
      clickHandlerFunc: buildTooltip(tooltipTexts['distribución'].text),
    },
  ],
}

// Sala 3
const sala3_a = {
  title: tar,
  type: 'equirectangular',
  panorama: 'data/img/tour/sala3_a.jpg',
  pitch: 1,
  yaw: -147,
  centerPitch: 1,
  centerYaw: -151,
  hotSpots: [
    {
      type: 'scene',
      sceneId: 'sala2_c',
      text: 'Sala 3',
      pitch: -10,
      yaw: 100,
      centerPitch: -8,
      centerYaw: -242,
    },
    {
      type: 'scene',
      sceneId: 'sala3_b',
      text: tar,
      pitch: -8,
      yaw: -149,
      centerPitch: -14,
      centerYaw: -162,
    },
  ],
}

const sala3_b = {
  title: tar,
  type: 'equirectangular',
  panorama: 'data/img/tour/sala3_b.jpg',
  hotSpots: [
    {
      type: 'scene',
      sceneId: 'sala3_a',
      text: tar,
      pitch: -8,
      yaw: 163,
      centerPitch: -7,
      centerYaw: -161,
    },
    {
      type: 'scene',
      sceneId: 'tar_a',
      text: tar,
      pitch: -21,
      yaw: -10,
      centerPitch: 0,
      centerYaw: -5,
    },
    {
      type: 'info',
      text: tooltipTexts['metal_a'].title,
      pitch: 15,
      yaw: -100,
      centerPitch: 8,
      centerYaw: 260,
      clickHandlerFunc: buildTooltip(tooltipTexts['metal_a'].text),
    },
  ],
}

// SALA Arq Tartarini
const tar_a = {
  title: tar,
  type: 'equirectangular',
  panorama: 'data/img/tour/tar_a.jpg',
  pitch: 0,
  yaw: -16,
  centerPitch: -5,
  centerYaw: -17,
  hotSpots: [
    {
      type: 'scene',
      sceneId: 'sala3_b',
      text: tar,
      pitch: -20,
      yaw: 130,
      centerPitch: -3,
      centerYaw: 119,
    },
    {
      type: 'scene',
      sceneId: 'tar_b',
      text: tar,
      pitch: -15,
      yaw: 22,
      centerPitch: 1,
      centerYaw: 21,
    },
    {
      type: 'scene',
      sceneId: 'tar_c',
      text: tar,
      pitch: -15,
      yaw: -16,
      centerPitch: 5,
      centerYaw: -15,
    },
    {
      type: 'scene',
      sceneId: 'tar_d',
      text: tar,
      pitch: -15,
      yaw: -54,
      centerPitch: 0,
      centerYaw: -4,
    },
    {
      type: 'scene',
      sceneId: 'tar_e',
      text: tar,
      pitch: -13,
      yaw: -87,
      centerPitch: -5,
      centerYaw: -53,
    },
    {
      type: 'scene',
      sceneId: 'colonial_a',
      text: 'Cuadra Colonial',
      pitch: -13,
      yaw: -123,
      centerPitch: -4,
      centerYaw: -91,
    },
    {
      type: 'scene',
      sceneId: 'pasillo_c',
      text: 'Pasillo',
      pitch: -9,
      yaw: 55,
      centerPitch: 11,
      centerYaw: 64,
    },
  ],
}

const tar_b = {
  title: tar,
  type: 'equirectangular',
  panorama: 'data/img/tour/tar_b.jpg',
  pitch: -4,
  yaw: -36,
  centerPitch: 0,
  centerYaw: 329,
  hotSpots: [
    {
      type: 'scene',
      sceneId: 'tar_a',
      text: tar,
      pitch: -5,
      yaw: -163,
      centerPitch: -4,
      centerYaw: 194,
    },
    {
      type: 'scene',
      sceneId: 'tar_c',
      text: tar,
      pitch: -10,
      yaw: -116,
      centerPitch: 3,
      centerYaw: -116,
    },
  ],
}

const tar_c = {
  title: tar,
  type: 'equirectangular',
  panorama: 'data/img/tour/tar_c.jpg',
  pitch: 0,
  yaw: -42,
  centerPitch: 6,
  centerYaw: -40,
  hotSpots: [
    {
      type: 'scene',
      sceneId: 'tar_a',
      text: tar,
      pitch: -6,
      yaw: 151,
      centerPitch: 11,
      centerYaw: -202,
    },
    {
      type: 'scene',
      sceneId: 'tar_b',
      text: tar,
      pitch: -9,
      yaw: 59,
      centerPitch: -2,
      centerYaw: 43,
    },
    {
      type: 'scene',
      sceneId: 'tar_d',
      text: tar,
      pitch: -9,
      yaw: -124,
      centerPitch: 2,
      centerYaw: -124,
    },
    {
      type: 'info',
      text: tooltipTexts['temporarias'].title,
      pitch: 16,
      yaw: -42,
      centerPitch: 5,
      centerYaw: -33,
      clickHandlerFunc: buildTooltip(tooltipTexts['temporarias'].text),
    },
  ],
}

const tar_d = {
  title: tar,
  type: 'equirectangular',
  panorama: 'data/img/tour/tar_d.jpg',
  pitch: -11,
  yaw: -52,
  centerPitch: -10,
  centerYaw: -48,
  hotSpots: [
    {
      type: 'scene',
      sceneId: 'tar_a',
      text: tar,
      pitch: -17,
      yaw: 98,
      centerPitch: -16,
      centerYaw: 120,
    },
    {
      type: 'scene',
      sceneId: 'tar_c',
      text: tar,
      pitch: -12,
      yaw: 49,
      centerPitch: -6,
      centerYaw: 48,
    },
    {
      type: 'scene',
      sceneId: 'tar_e',
      text: tar,
      pitch: -24,
      yaw: 159,
      centerPitch: -4,
      centerYaw: -194,
    },
  ],
}

const tar_e = {
  title: tar,
  type: 'equirectangular',
  panorama: 'data/img/tour/tar_e.jpg',
  hotSpots: [
    {
      type: 'scene',
      sceneId: 'tar_a',
      text: tar,
      pitch: -10,
      yaw: 101,
      centerPitch: 1,
      centerYaw: 72,
    },
    {
      type: 'scene',
      sceneId: 'tar_d',
      text: tar,
      pitch: -14,
      yaw: -20,
      centerPitch: -4,
      centerYaw: -9,
    },
    {
      type: 'scene',
      sceneId: 'colonial_a',
      text: 'Cuadra Colonial',
      pitch: -18,
      yaw: 143,
      centerPitch: -1,
      centerYaw: 207,
    },
    {
      type: 'scene',
      sceneId: 'colonial_b',
      text: 'Cuadra Colonial',
      pitch: -14,
      yaw: -116,
      centerPitch: -3,
      centerYaw: -117,
    },
    {
      type: 'scene',
      sceneId: 'colonial_c',
      text: 'Cuadra Colonial',
      pitch: -13,
      yaw: -76,
      centerPitch: -2,
      centerYaw: -108,
    },
    {
      type: 'scene',
      sceneId: 'pasillo_c',
      text: 'Pasillo',
      pitch: -2,
      yaw: 59,
      centerPitch: 6,
      centerYaw: 57,
    },
  ],
}

const colonial_a = {
  title: 'Cuadra Colonial',
  type: 'equirectangular',
  panorama: 'data/img/tour/colonial_a.jpg',
  pitch: -6,
  yaw: -23,
  centerPitch: 0,
  centerYaw: -23,
  hotSpots: [
    {
      type: 'scene',
      sceneId: 'tar_a',
      text: tar,
      pitch: -8,
      yaw: -128,
      centerPitch: -5,
      centerYaw: -96,
    },
    {
      type: 'scene',
      sceneId: 'tar_e',
      text: tar,
      pitch: -16,
      yaw: 164,
      centerPitch: -2,
      centerYaw: -199,
    },
    {
      type: 'scene',
      sceneId: 'colonial_b',
      text: 'Cuadra Colonial',
      pitch: -7,
      yaw: 108,
      centerPitch: -1,
      centerYaw: -226,
    },
  ],
}

const colonial_b = {
  title: 'Cuadra Colonial',
  type: 'equirectangular',
  panorama: 'data/img/tour/colonial_b.jpg',
  hotSpots: [
    {
      type: 'scene',
      sceneId: 'tar_e',
      text: tar,
      pitch: -8,
      yaw: 154,
      centerPitch: -2,
      centerYaw: -206,
    },
    {
      type: 'scene',
      sceneId: 'colonial_a',
      text: 'Cuadra Colonial',
      pitch: -10,
      yaw: -175,
      centerPitch: 4,
      centerYaw: -209,
    },
    {
      type: 'scene',
      sceneId: 'colonial_c',
      text: 'Cuadra Colonial',
      pitch: -5,
      yaw: 123,
      centerPitch: 1,
      centerYaw: -207,
    },
    {
      type: 'info',
      text: tooltipTexts['colonial'].title,
      pitch: 15,
      yaw: -25,
      centerPitch: 2,
      centerYaw: -22,
      clickHandlerFunc: buildTooltip(tooltipTexts['colonial'].text),
    },
  ],
}

const colonial_c = {
  title: 'Cuadra Colonial',
  type: 'equirectangular',
  panorama: 'data/img/tour/colonial_c.jpg',
  pitch: -1,
  yaw: 18,
  centerPitch: -7,
  centerYaw: 18,
  hotSpots: [
    {
      type: 'scene',
      sceneId: 'colonial_a',
      text: 'Cuadra Colonial',
      pitch: -8,
      yaw: -122,
      centerPitch: -1,
      centerYaw: -107,
    },
    {
      type: 'scene',
      sceneId: 'colonial_b',
      text: 'Cuadra Colonial',
      pitch: -12,
      yaw: -89,
      centerPitch: 6,
      centerYaw: -116,
    },
    {
      type: 'scene',
      sceneId: 'tar_e',
      text: tar,
      pitch: -11,
      yaw: -143,
      centerPitch: 0,
      centerYaw: -119,
    },
  ],
}

// PASILLO
const pasillo_a = {
  title: 'Pasillo',
  type: 'equirectangular',
  panorama: 'data/img/tour/pasillo_a.jpg',
  pitch: -7,
  yaw: -122,
  centerPitch: -2,
  centerYaw: 235,
  hotSpots: [
    {
      type: 'scene',
      sceneId: 'pasillo_b',
      text: 'Balcón',
      pitch: -5,
      yaw: -123,
      centerPitch: -12,
      centerYaw: -99,
    },
    {
      type: 'scene',
      sceneId: 'archivo_a',
      text: 'Archivo',
      pitch: -13,
      yaw: -15,
      centerPitch: -10,
      centerYaw: -26,
    },
    {
      type: 'scene',
      sceneId: 'pasillo_c',
      text: 'Pasillo',
      pitch: -3,
      yaw: 71,
      centerPitch: 0,
      centerYaw: -291,
    },
  ],
}

const pasillo_b = {
  title: 'Pasillo',
  type: 'equirectangular',
  panorama: 'data/img/tour/pasillo_b.jpg',
  pitch: -15,
  yaw: -28,
  centerPitch: 0,
  centerYaw: -27,
  hotSpots: [
    {
      type: 'scene',
      sceneId: 'balcon',
      text: 'Balcón',
      pitch: -5,
      yaw: 151,
      centerPitch: 8,
      centerYaw: -203,
    },
    {
      type: 'scene',
      sceneId: 'pasillo_a',
      text: 'Pasillo',
      pitch: -2,
      yaw: -26,
      centerPitch: 1,
      centerYaw: -21,
    },
  ],
}

const pasillo_c = {
  title: 'Pasillo',
  type: 'equirectangular',
  panorama: 'data/img/tour/pasillo_c.jpg',
  pitch: -1,
  yaw: -26,
  centerPitch: 2,
  centerYaw: -27,
  hotSpots: [
    {
      type: 'scene',
      sceneId: 'pasillo_a',
      text: 'Pasillo',
      pitch: 0,
      yaw: -19,
      centerPitch: -6,
      centerYaw: -33,
    },
    {
      type: 'scene',
      sceneId: 'tar_a',
      text: tar,
      pitch: -5,
      yaw: -125,
      centerPitch: -2,
      centerYaw: -124,
    },
    {
      type: 'info',
      text: tooltipTexts['metal_b'].title,
      pitch: 0,
      yaw: -27,
      centerPitch: 0,
      centerYaw: -25,
      clickHandlerFunc: buildTooltip(tooltipTexts['metal_b'].text),
    },
  ],
}

const balcon = {
  title: 'Balcón',
  type: 'equirectangular',
  panorama: 'data/img/tour/balcon.jpg',
  hotSpots: [
    {
      type: 'scene',
      sceneId: 'pasillo_b',
      text: 'Pasillo',
      pitch: -5,
      yaw: 3,
      centerPitch: 0,
      centerYaw: 0,
    },
    {
      type: 'info',
      text: tooltipTexts['balcon'].title,
      pitch: 22,
      yaw: 3,
      centerPitch: 11,
      centerYaw: 1,
      clickHandlerFunc: buildTooltip(tooltipTexts['balcon'].text),
    },
  ],
}

// ARCHIVO

const archivo_a = {
  title: 'Archivo',
  type: 'equirectangular',
  panorama: 'data/img/tour/archivo_a.jpg',
  pitch: -16,
  yaw: -38,
  centerPitch: 4,
  centerYaw: -31,
  hotSpots: [
    {
      type: 'scene',
      sceneId: 'pasillo_a',
      text: 'Pasillo',
      pitch: -17,
      yaw: 86,
      centerPitch: 0,
      centerYaw: 53,
    },
    {
      type: 'scene',
      sceneId: 'archivo_b',
      text: 'Archivo',
      pitch: -6,
      yaw: -33,
      centerPitch: -10,
      centerYaw: -24,
    },
    {
      type: 'scene',
      sceneId: 'archivo_d',
      text: 'Archivo',
      pitch: -20,
      yaw: -121,
      centerPitch: -2,
      centerYaw: -120,
    },
  ],
}

const archivo_b = {
  title: 'Pasillo',
  type: 'equirectangular',
  panorama: 'data/img/tour/archivo_b.jpg',
  pitch: -11,
  yaw: 157,
  centerPitch: -3,
  centerYaw: 155,
  hotSpots: [
    {
      type: 'scene',
      sceneId: 'archivo_a',
      text: 'Archivo',
      pitch: -6,
      yaw: 156,
      centerPitch: -11,
      centerYaw: 157,
    },
    {
      type: 'scene',
      sceneId: 'archivo_d',
      text: 'Archivo',
      pitch: -15,
      yaw: -168,
      centerPitch: -11,
      centerYaw: 182,
    },
  ],
}

const archivo_c = {
  title: 'Archivo',
  type: 'equirectangular',
  panorama: 'data/img/tour/archivo_c.jpg',
  pitch: -5,
  yaw: 152,
  centerPitch: 0,
  centerYaw: -208,
  hotSpots: [
    {
      type: 'scene',
      sceneId: 'archivo_d',
      text: 'Archivo',
      pitch: 2,
      yaw: 152,
      centerPitch: -5,
      centerYaw: 166,
    },
  ],
}

const archivo_d = {
  title: 'Archivo',
  type: 'equirectangular',
  panorama: 'data/img/tour/archivo_d.jpg',
  pitch: 15,
  yaw: 118,
  centerPitch: 3,
  centerYaw: 118,
  hotSpots: [
    {
      type: 'scene',
      sceneId: 'archivo_c',
      text: 'Archivo',
      pitch: 1,
      yaw: 118,
      centerPitch: -5,
      centerYaw: 124,
    },
    {
      type: 'scene',
      sceneId: 'archivo_a',
      text: 'Archivo',
      pitch: -17,
      yaw: 26,
      centerPitch: -4,
      centerYaw: 35,
    },
    {
      type: 'info',
      text: tooltipTexts['archivo'].title,
      pitch: 7,
      yaw: -155,
      centerPitch: 3,
      centerYaw: 209,
      clickHandlerFunc: buildTooltip(tooltipTexts['archivo'].text),
    },
  ],
}

const obj = {
  default: {
    firstScene: 'entrada_a',
    // hotSpotDebug: true,
    autoLoad: true,
    sceneFadeDuration: 500,
    autoRotate: -1,
    autoRotateStopDelay: 1,
    autoRotateInactivityDelay: 7000,
    hfov: 120 /* 120 es el maximo */,
  },
  scenes: {
    entrada_a,
    entrada_b,
    sala1_a,
    sala1_b,
    sala1_c,
    sala2_a,
    sala2_b,
    sala2_c,
    sala3_a,
    sala3_b,
    tar_a,
    tar_b,
    tar_c,
    tar_d,
    tar_e,
    colonial_a,
    colonial_b,
    colonial_c,
    pasillo_a,
    pasillo_b,
    pasillo_c,
    archivo_a,
    archivo_b,
    archivo_c,
    archivo_d,
    balcon,
  },
}

var scene = pannellum.viewer('panorama', obj)

function sceneChangeListener() {
  /*
  console.log('Scene changed')
  console.log('Scene ' + scene.getScene())
  console.log('Yaw ' + scene.getYaw())
  console.log('Pitch ' + scene.getPitch())
  console.log('Hfov ' + scene.getHfov())
  */
  scene.lookAt(
    scene.getPitch(),
    scene.getYaw(),
    115,
    3000,
    sceneLoadListener,
    {}
  )
}

function sceneLoadListener() {}

scene.on('scenechange', sceneChangeListener)
